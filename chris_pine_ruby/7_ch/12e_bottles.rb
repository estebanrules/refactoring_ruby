# “99 Bottles of Beer on the Wall.” Write a program that prints out
# the lyrics to that beloved classic, “99 Bottles of (Root) Beer on the Wall.”
# Version Two - with a block.

100.times do |n|
  puts "#{n} bottles of beer of on the wall!"
  puts "#{n} bottles of beeeeeer!"
  puts "Take one down, pass it around, #{n} bottles of beer on the wall!"
end

# I'm befuddled.  How can I make this block "count backwards"?

