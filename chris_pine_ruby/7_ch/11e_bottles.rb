# “99 Bottles of Beer on the Wall.” Write a program that prints out
# the lyrics to that beloved classic, “99 Bottles of (Root) Beer on the Wall.”
# Version One - with a while loop.

def plural(value)
  value == 1 && '' || 's'
end

def bottles_of_beer(num = 99)
  until num == -1
    puts "#{num} bottles of beer on the wall."
    puts "#{num} bottles of beeeeeer!"
    puts "Take one down, pass it around, #{num} bottle#{plural(num)} of beer on the wall!"
    num -= 1
  end
end

bottles_of_beer 1
