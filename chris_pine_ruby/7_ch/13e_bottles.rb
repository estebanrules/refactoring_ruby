# “99 Bottles of Beer on the Wall.” Write a program that prints out
# the lyrics to that beloved classic, “99 Bottles of (Root) Beer on the Wall.”
# Version 3 - as a class.
class BeerSong
  def initialize(num_bottles)
    @num_bottles = num_bottles
  end

  def bottles_left?
    @num_bottles != -1
  end

  def song
    while self.bottles_left?
      puts "#{@num_bottles} bottles of beer on the wall!"
      puts "#{@num_bottles} bottles of beeeeeer!"
      puts "Take one down, pass it around, #{@num_bottles} bottles of beer on the wall!"
      @num_bottles -= 1
    end
  end
end

bottles_of_beer = BeerSong.new(99)
bottles_of_beer.song


