# Reading a random line of text from a file, solution #1.
# Here we are converting the lines of text in to an array via the split method,
# then calling the sample method on the array.
text_array = File.foreach('quotes.txt').map { |line| line.split(' ') }
puts text_array.sample

# This is Vic's solution.
# Here we are using the IO#readlines method which does indeed store the result in an array.
text_array = IO.readlines('quotes.txt')
# This line is an example of self-testing code.  We are checking right here if
# we have what we expect
fail 'We really think we are getting an Array, but we are not.' unless text_array.is_a? Array
puts text_array.sample
