
# Reading a random line from a file, solution #2.
# Once again we are using the split method and the
# ampersand operator

text_array = File.readlines('quotes.txt').map &:split
puts text_array.sample
