# another way to shuffle an array.

my_array = ["Stallman and Steele", "Bill Joy", "Skinner", "Moolenaar"]

puts my_array.sort { |a, b| rand <=> rand }

