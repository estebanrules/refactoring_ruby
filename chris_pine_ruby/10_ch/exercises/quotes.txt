"The computer is a moron." - Peter Drucker
"To err is human - and to blame it on a computer is even more so." - Robert Orben
"Yeah, microchips, but what... is it good for?"  - an IBM senior engineer, 1968
"Reach out and grep someone." - Bell Labs Unix
"Linux is not portable." - Linus Torvalds
"640k is enough for anyone, and by the way, what's a network?" - William Gates III, President of Microsoft Corporation, 1984.
“C makes it easy to shoot yourself in the foot; C++ makes it harder, but when you do, it blows away your whole leg.” - Bjarne Stroustrup
“Most good programmers do programming not because they expect to get paid or get adulation by the public, but because it is fun to program.” - Linus Torvalds
“Any fool can write code that a computer can understand. Good programmers write code that humans can understand.” - Martin Fowler
“One of my most productive days was throwing away 1000 lines of code.” - Ken Thompson
"If debugging is the process of removing software bugs, then programming must be the process of putting them in." - Edsger Dijkstra
"Perl – The only language that looks the same before and after RSA encryption." - Keith Bostic
"I invented the term ‘Object-Oriented’, and I can tell you I did not have C++ in mind." - Alan Kay
"Controlling complexity is the essence of computer programming." - Brian Kernighan
