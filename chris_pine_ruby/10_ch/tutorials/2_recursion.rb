# Because there seems to be some universal law about introducing recursion
# must involve computing factorials, let's do it.
def factorial(num)
  if num < 0
    return 'You can\'t take the factorial of a negative number (actually you can but whatever)'
  end

  if num <= 1
    1
  else
    num * factorial(num - 1)
  end
end

puts factorial(10)
puts factorial(8)


