# Recursion (uh-oh)
# If you write a method that calls itself...that's an example of recursion, baby!
# Example:  Let's look at what our 'psych program' would look like 'with recursion' instead
# of 'with while loops'

def ask_recursively(question)
  puts question
  reply = gets.chomp.downcase

  if reply == 'yes'
    true
  elsif reply == 'no'
    false
  else
    puts 'Please answer "yes" or "no".'
    ask_recursively(question)
  end
end

ask_recursively('Do you wet the bed?')

