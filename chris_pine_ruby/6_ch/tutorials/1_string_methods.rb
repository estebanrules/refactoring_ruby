# Chapter 6 - String methods

# Reverse
var1 = 'stop'
var2 = 'deliver repaid desserts'
var3 = '....TCELES B HSUP  A  magic spell?'

puts var1.reverse
puts var2.reverse
puts var3.reverse
puts var1 # -> stop
puts var2 # -> deliver repaid desserts
puts var3 # -> '....TCELES B HSUP  A  magic spell?
# The reverse method does not change the string, it just makes a new 'backward'
# version of it.

# Length
puts "What is your full name?"
name = gets.chomp
puts "Did you know there are #{name.length} characters"
puts "In your name, #{name}?"

# Case
letters = 'aAbBcCDeE'
puts letters.upcase
puts letters.downcase
puts letters.swapcase
puts letters.capitalize
puts ' a'.capitalize # output is: a.  as you can see here, it only capitalizes the first CHARACTER. not LETTER
puts letters

# The center, ljust and rjust methods
# If I wanted to center the lines of a poem:
line_width = 50
puts                   'Old Mother Hubbard'.center(line_width)
puts                  'Sat in her cupboard'.center(line_width)
puts           'Eating her wurds and whey,'.center(line_width)
puts             'When along came a spider'.center(line_width)
puts             'Who sat down besider her'.center(line_width)
puts    'And scared her poor shoe dog away.'.center(line_width)

# See 6ch2.rb



















































