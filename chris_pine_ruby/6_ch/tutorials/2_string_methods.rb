# The above looks pretty good, but you can use ljust and rjust (left-justify, right-justify)
# which are similar to center, except that they pad the string with spaces on the left
# and right sides, respectively.
# Example:
line_width = 40
str = '--> text <--'
puts str.ljust( line_width)
puts str.center(line_width)
puts str.rjust( line_width)
puts str.ljust(line_width / 2) + str.rjust(line_width / 2)

