# Write an Angry Boss program.
# It should rudely ask what you want. Whatever you answer,
# the Angry Boss should yell it back to you, and then fire you.
# For example, if you type in 'I want a raise',
# it should yell back 'WHADDAYA MEAN "I WANT A RAISE."?!?  YOU'RE FIRED!!'

# Yellable has the yell method, which takes a message and upcases it.
module Yellable
  def yell(message)
    message.upcase
  end
end
# So what does our 'Boss' do?
# 1.) He listens (ok, not very well, but still) and gets input.
# 2.) He yells back.
# doctest: Boss.new.yell('hi!')
# >> Boss.new.yell('hi!')
# => "WHADDYA MEAN 'HI!'?!?!? YOU'RE FIRED!"
class Boss
  include Yellable
  alias_method :old_yell, :yell
  def yell(message)
    old_yell("WHADDYA MEAN '#{message}'?!?!? YOU'RE FIRED!")
  end
end

# doctest: Customer can yell
# >> Customer.new.yell("I want my cheeseburger now!")
# => "I WANT MY CHEESEBURGER NOW!"
# doctest: Boss responds with his quote when given a statement from customer
# >> Boss.new.yell(Customer.new.yell('I want my cheeseburger now!'))
# => "WHADDYA MEAN 'I WANT MY CHEESEBURGER NOW!'?!?!? YOU'RE FIRED!"

class Customer
  include Yellable
end

if __FILE__ == $PROGRAM_NAME
  angry_boss = Boss.new
  puts 'What would you like to say to your Angry Boss?'
  input = gets.chomp
  puts angry_boss.yell input
end
