# More Array Methods
web_frameworks = ['Martini', 'Rails', 'Camping', 'Seaside', 'Phoenix']

puts web_frameworks
puts
puts web_frameworks.to_s
puts web_frameworks.join(', ')
puts
puts web_frameworks.join('  :) ') + ' 8)'

200.times do
  puts []
end

# Output:

# Martini
# Rails
# Camping
# Seaside
# Phoenix

# ["Martini", "Rails", "Camping", "Seaside", "Phoenix"]
# Martini, Rails, Camping, Seaside, Phoenix

# Martini  :) Rails  :) Camping  :) Seaside  :) Phoenix 8)

# RUBY TREATS ARRAYS DIFFERENTLY FROM OTHER OBJECTS.  With arrays, puts calls
# puts on each of the objects in the array, and there were no objects, thus
# no output.

