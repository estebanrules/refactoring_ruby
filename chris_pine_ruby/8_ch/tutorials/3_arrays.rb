# Array Methods - push, pop, last.
# Think of push and pop as 'opposites'.
# push adds an object to the end of an array, and pop removes the last object
# from the array (and tells you what it was).
# The method last is similar to pop in that it tells you what's at the end of
# the array, except that it leaves the array alone.
# Again, push and pop actually CHANGE the array.

important_guys = []

important_guys.push('Alan Kay')
important_guys.push('Dennis Ritchie')
important_guys.push('Guido van Rossum')

puts important_guys[0]
puts important_guys.last
puts important_guys.length

puts "Sorry Python, but I have chosen to pop #{important_guys.pop} (not literally)"

puts "So we're left with #{important_guys}."
puts important_guys.length


