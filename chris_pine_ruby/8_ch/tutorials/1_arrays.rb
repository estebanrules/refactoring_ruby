# Arrays - Chapter 8
# Examples:
names = ['Ada', 'Bello', 'Esteban']

puts names
puts
puts names[0]
puts names[1]
puts names[2]
puts names[3] # out of range, nil

# You don't have to assign to the slots in any particular order,
# and any you leave empty are filled with nil by default.
others = []

others[3] = 'Davey'
others[0] = 'Mikey'
others[1] = 'Seedee'
others[0] = 'Mikey Mikey'

puts others
puts

# Method each
proglangs = ['Ruby', 'Elixir', 'Smalltalk', 'Fortran', 'io']

proglangs.each do  |lang|
  puts 'I love ' + lang + '!'
  puts 'Don\'t you?'
end

puts '...and Java?!?!'
puts '...no one?'
# Let's break this down in to English.  for each object in proglangs,
# point the variable 'lang' to the object, and then do everything I tell you
# to, until you come to the end.
# do and end specify a block of code.




