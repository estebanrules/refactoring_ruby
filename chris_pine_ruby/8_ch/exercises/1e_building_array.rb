# Building and sorting an array. Write the program we talked about at
# the beginning of this chapter, one that asks us to type as many
# we want, (one word per line, continuing until we just press enter),
# and then repeats the words in alphabetical order.

puts "Input as many words as you would like.
  When finished, press enter."

my_array = Array.new

input = gets.chomp

until input == ''
  my_array.push(input)
  input = gets.chomp
end

puts my_array.sort


