# Writing your own Methods - Chapter 9

while true
  puts 'Do you like tacos?'
  answer = gets.chomp
  if (answer == 'yes' || answer == 'no')
    break
  else
    puts 'Answer "yes" or "no".'
  end
end

# Pay attention to this one
while true
  puts 'Do you wet the bed?'
  answer = gets.chomp.downcase
  if (answer == 'yes' || answer == 'no') # if it equals yes OR no, then go to the nested statement
    if answer == 'yes'
      wets_bed = true # here we assign a variable a value of true or false.
    else
      wets_bed = false
    end
    break
  else # else, do this:
    puts 'Please answer "yes" or "no".'
  end
end

while true
  puts 'Do you like eating chimis?'
  answer = gets.chomp
  if (answer == 'yes'|| answer == 'no')
    break
  else
    puts 'Please answer "yes" or "no".'
  end
end

# What's the point of all this crap?  Avoid repetition (but isn't life by definition repetitive?)





