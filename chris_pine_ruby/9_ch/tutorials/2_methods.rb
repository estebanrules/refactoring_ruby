# Local variables in methods
def double_this_num(num)
  num_times2 = num * 2
  puts num.to_s + ' doubled is ' + num_times2.to_s
end

double_this_num(44)
puts num_times2.to_s # this throws an error?  Why?  it's a local variable to the method (duh)
                     # it doesn't "exist" outside of it.

puts 16 * 9

class Dog
  def initialize(name)
    @name = name
  end
end
