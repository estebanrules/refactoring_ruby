# Chris Pine - Learn to Program
# Instance Variables - instance variables last as long as the object doesm as
# opposed to method variables which only last as long as the method does.

# Here we use initialize to "setup out object right when its created"
class Die
  attr_reader :sides, :showing

  def initialize(sided_die = 6)
    @sides = sided_die
  end

  def roll
    @showing = rand(1..@sides) # random can take a range, so take advantage
  end

  def to_s
    if showing
      showing.to_s
    else
      'you forgot to roll the dice, they are still in the cup!'
    end
  end

  def +(other)
    showing + other.showing
  end

  # returns lambda, the argument to call should be a collection of Die objects,
  # and those die objects must have been rolled.
  def self.total
    ->(cup) { cup.reduce(0) { |a, e| a + e.showing } }
  end
end

# A cup of dice the hard way
cup_of_dice = [Die.new, Die.new, Die.new, Die.new, Die.new, Die.new(20)]
vics_cup_of_dice = []
6.times { vics_cup_of_dice << Die.new }
puts "Vic's cup of dice: "
puts vics_cup_of_dice
puts "Now Rolling Vic's cup of dice:"
vics_cup_of_dice.each { |v| v.roll }
puts 'Rolling my cup of dice!'
cup_of_dice.each do |d|
  puts "#{d.sides} sided die: #{d.roll}"
end

puts 'Proof that die are held will be coming momentarily!'

cup_of_dice.each do |d|
  puts "The #{d.sides} sided die as rolled is still #{d}"
end

puts "Vic's cup of dice"
vics_cup_of_dice.each do |v|
  puts "#{v.sides} sided : #{v}"
end

puts "For a total of #{ Die.total[vics_cup_of_dice] }."
