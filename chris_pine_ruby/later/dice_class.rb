# Chris Pine - Learn to Program
# Creating Classes

class Die
  def roll
    1 + rand(7) # class method roll will return rand(7) + 1
  end
end

# Let's instantiate some objects.  Here we instantiate objects in to an array (interesting)
dice = [Die.new, Die.new, Die.new]

# We call the class method roll, but we call it via iterating over the above array of newly
# instantiated objects we just created

dice.each do |die|
  puts die.roll
end


class Dog
  def initialize(name)
    @name = name
  end

  def bark
    "Woof, woof."
   end
end

fido = Dog.new("fido")
fido.bark
