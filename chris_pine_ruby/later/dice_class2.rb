# Chris Pine - Learn to Program
# Instance Variables - instance variables last as long as the object doesm as opposed to method
# variables which only last as long as the method does.
class Die
  CUP_OF_DICE = []
  def roll
    @number_showing = 1 + rand(7) # we assign our instance variable to 1 + rand(7)
  end

  def showing
    @number_showing # this method simply returns the value of the instance variable
  end

  def generate_dice(num_dice)
    num_dice.to_i.times do
      CUP_OF_DICE << Die.new
    end
  end
end

d1 = Die.new
puts CUP_OF_DICE
puts "How many dice would you like to generate?"
num_dice = gets.chomp

d1.generate_dice(num_dice)

puts CUP_OF_DICE

