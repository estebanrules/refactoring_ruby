# Write a function, time_for_milk_and_cookies that accepts a Date object,
# and returns true if it's Christmas Eve (December 24th), false otherwise.
# Examples:
# time_for_milk_and_cookies( Date.new( 2013, 12, 24 ) ) # December 24, 2013 => returns true
# time_for_milk_and_cookies( Date.new( 2013, 1, 23 ) ) # January 23, 2013 => returns false
# time_for_milk_and_cookies( Date.new( 3000, 12, 24 ) ) # December 24, 3000 => returns true
require 'date'
def time_for_milk_and_cookies(date)
  christmas_eve = Date.new(2014, 12, 24)
  christmas_eve.day == date.day && christmas_eve.month == date.month
end

puts time_for_milk_and_cookies(Date.new(2013, 12, 24))
puts time_for_milk_and_cookies(Date.new( 2013, 1, 23 ))
puts time_for_milk_and_cookies(Date.new( 3000, 12, 24 ))

