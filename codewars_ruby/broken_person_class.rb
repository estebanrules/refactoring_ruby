# The following code was thought to be working properly,
# however when the code tries to access the age of the person instance it fails.
# Fix it.
# -----------
# Note: The above comments are from the codewars, the rest are mine.

class Person
  def initialize(firstName, lastName, age) # First of all, this Camel Case looks nasty.
    @firstName = firstName
    @lastName = lastName
    @age = age
  end

  def full_name
    "#{@firstName} #{@lastName}"
  end
end

person = Person.new('Yukihiro', 'Matsumoto', 47)
puts person.full_name
puts person.age

# To "fix it" we could write a method like so:
# def age
#   "#{@age}"
# end

# We could also call the attr_reader method before the class initialize method:
# class Person
#   attr_reader :age
#   def initialize
#     # blah blah
#   end

# Let's do the latter.
