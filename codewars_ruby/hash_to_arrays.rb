# Complete the keys_and_values function so that
# it takes in an object and returns the keys
# and values as separate arrays.

def keys_and_values(data)
  keys, values = data.keys, data.values
end

data = { Matsumoto: "Ruby", Ritchie: "C", Wirth: "Pascal", Backus: "Fortran", McCarthy: "Lisp" }

puts keys_and_values(data)

# Admittedly, I had to ask about this one one on Stack Overflow.
# I figured out how to get an array of keys or values, like so:
# data.map { |creator, proglang| creator }
# But couldn't get it to make one for each.

# Other solutions I found on codewars:
#
# def keysAndValues(data)
#   [data.keys, data.values]
# end
#
# def keysAndValues(data)
#   data.to_a.transpose
# end
#
# def keysAndValues(data)
#   [] << data.keys << data.values
# end
#
# def keysAndValues(data)
#   Array.new << data.keys << data.values
# end
def meth
  puts "The Dick is red"
end
