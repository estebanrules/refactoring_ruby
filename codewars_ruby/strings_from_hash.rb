# Complete the solution so that it takes the hash passed in and
# generates a human readable string from its key/value pairs.
# The format should be "KEY = VALUE". Each key/value pair should be separated
# by a comma except for the last pair.
# Example: solution({a: 1, b: '2'}) # should return "a = 1,b = 2"
def solution(pairs)
  pairs.map { |letter, number| puts "#{letter} = #{number}" }.join(',')
end

pairs = {
  :a => 1,
  :b => '2'
}

solution(pairs)

# I'm having some trouble with this, specifically 'each key/value pair should be
# separated by a comma except for the last pair.'  Not sure how to format that.

