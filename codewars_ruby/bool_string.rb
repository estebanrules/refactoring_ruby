# Complete the bool_to_word method.

# Given: a boolean value
# Return: a 'Yes' string for true and a 'No' string for false
def bool_to_word(bool)
  if bool == true
    'Yes'
  else
    'No'
  end
end
