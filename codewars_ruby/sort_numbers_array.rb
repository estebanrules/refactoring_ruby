# Finish the solution so that it sorts the passed in array of numbers. If the function passes
# in an empty array or null/nil value then it should return an empty array.

# solution([1, 2, 10, 50, 5]) -> [1,2,5,10,50]
# solution(nil) -> []

def solution(nums)
  if nums.to_a.empty?
    return Array.new
  else
    nums.sort
  end
end

# Or we could do it this way.  Much better.
def solution_two(nums)
  Array(nums).sort
end
