# We can convert symbols to strings and vice-a-versa with the methods to_s and to_sym
# We have an array of strings we'd like to later use as hash keys, but we'd rather they be symbols.
# 1 - Create a new variable, symbols, and store an empty array in it.
# 2 - Use .each to iterate over the strings array.
# 3 - For each s in strings, use .to_sym to convert s to a symbol
#  and use .push to add that new symbol to symbols.

strings = ["Smalltalk", "Lua", "Perl", "io", "Ruby"]
# symbols = []

# Here is one way to do this:
# strings.each { |s| symbols.push s.to_sym }

# Or:
# symbols = strings.map { |s| s.to_sym }

# Or as of Ruby 1.9:
symbols = strings.map &:to_sym

# Or we cuold use the intern method, which from what I understand
# is identical to the to_sym method.
symbols = strings.map &:intern


# So, how does this work?
# 1 - & tells Ruby that the thing that follows it is a Proc.
# 2 - the symbol :to_sym is converted to a Proc using the .to_proc method of the Symbol class.

#-----------------------------------------------------------------------
# Question for Vic - This exercise was good, I'm starting to get a better
# handle on the use of the & method.  But (there is always a 'but', isn't
# there?) can you give me one "real-world" application of this?
#-----------------------------------------------------------------------

