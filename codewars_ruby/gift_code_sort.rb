# Santa's senior gift organizer Elf developed a way to represent
# up to 26 gifts by assigning a unique alphabetical character to each gift.
# After each gift was assigned a character, the gift organizer Elf then joined
# the characters to form the gift ordering code.
# Santa asked his organizer to order the characters in alphabetical order, but the
# Elf fell asleep from consuming too much hot chocolate and candy canes! Can you
# help him out?
#
# Write a function called sortGiftCode (sort_gift_code in Ruby) that
# accepts a string containing up to 26 unique alphabetical characters, and returns
# a string containing the same characters in alphabetical order.
def sort_gift_code code
  code.chars.sort.join
end

# We use the String method chars, which returns an enumeration of the string's
# characters, then the Enumerable method sort, then join to convert it from an
# enumeration to a string.

# From stackoverflow, using the & operator:
# str.chars.sort(&:casecmp).join
# and:
# str.chars.sort_by(&:downcase).join

