# Ruby blocks (closures in other languages) are chunks of code between curly braces or
# between do/end that you can associate with method invocations, as if they were parameters.
# Matz says that ANY METHOD can be called with a block as an implicit argument, inside
# the method, you can call the block using the yield keyword with a value.

# A method can have an associated block one or more times using the Ruby yield statement.
# Thus, any method that wants to take a block as a parameter can use the yield keyword
# to execute the block at any time.
# Example:
def call_block
  puts 'Start of method'
  # Below we call the block using the yield method
  yield
  yield
  puts 'End of method'
end
# A code block can only appear "below" a method call.
# Now we call our method, passing in our block:
call_block {puts 'In the block'}

# If no code block is given, Ruby raises an exception.
# call_block
# when we call this method without passing a block in to it, we get
# the following error:
# lock_tutorial.rb:13:in `call_block': no block given (yield) (LocalJumpError)

# You can provide parameters to the call to yield: these will be passed to
# to the block.  Within the block, you list the names of the arguments to reveice
# the parameters between vertical bars. ||
# Example:
def call_block_two
  yield('hello', 99)
end
call_block_two { |str, num| puts str + ' ' + num.to_s }

