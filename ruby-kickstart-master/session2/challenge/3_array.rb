# Write a method named every_other_char for strings that, 
# returns an array containing every other character
# 
# example:
# "abcdefg".every_other_char  # => "aceg"
# "".every_other_char         # => ""

# def every_other_char(str)
#   str.select.each_with_index { |str, i| i.odd? }
# end
class String
  def every_other_char(str)
    str.split(' ').select.each_with_index { |str, i| i.odd? }
  end
end
