# fill out the method below, then run the tests with
#   $ rake 1:2


# Given two numbers, a and b, return half of whichever is smallest, as a float
#
# arithmetic2(1,2)    # => 0.5
# arithmetic2(19,10)  # => 5.0
# arithmetic2(-6,-7)  # => -3.5

def arithmetic2(a,b)
  if a < b
    a * 0.5
  else
    b * 0.5
  end
end

# This was their solution:
# def arithmetic2(a,b)
#   min = if a < b
#     a
#   else
#     b
#   end
#
#   min / 2.0
#
# end
#
# Is this more efficient?
