# You'll get a string and a boolean.
# When the boolean is true, return a new string containing all the odd characters.
# When the boolean is false, return a new string containing all the even characters.
#
# If you have no idea where to begin, remember to check out the cheatsheets for string and logic/control
#
# odds_and_evens("abcdefg",true)    # => "bdf"
# odds_and_evens("abcdefg",false)   # => "aceg"

def odds_and_evens(string, return_odds)
  if return_odds == true
    string.split("").select.each_with_index {|str, i| i.odd? }.join("")
  elsif return_odds == false
    string.split("").select.each_with_index { |str, i| i.even? }.join("")
  end
end

# Their solution:
# def odds_and_evens(string, return_odds)
#   to_return = ""
#   string.size.times do |index|
#     if (return_odds && index.odd?) || (!return_odds && index.even?)
#       to_return << string[index]
#     end
#   end
#   to_return
# end


# Thoughts / Notes to Vic:
# This took me quite awhile to figure out.  I need to go through
# class Enumerable and ALL of its methods.
#
# Am I correct in thinking my solution is "better" than theirs?
