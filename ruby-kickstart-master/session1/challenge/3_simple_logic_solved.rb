# remember, you can test this file with
#   $ rake 1:3


# Given a number, n, return 10 if it is even, and 20 if it is odd
#
# ten_twenty(5) # => 20
# ten_twenty(6) # => 10

def ten_twenty(n)
  if n % 2 != 0
    20
  else
    10
  end
end

# I decided to write a class method to check if a number is even.
class Fixnum
  def is_even?
    self % 2 == 0
  end
end

my_number = 6

puts my_number.is_even?

# Here is their solution:
#  def ten_twenty(n)
#   if n % 2 == 0
#     10
#    else
#      20
#    end
#  end
#
