# Given a string, replace every instance of sad to happy
#
# add_more_ruby("The clowns were sad.")         # => "The clowns were happy."
# add_more_ruby("The sad dad said sad stuff.")  # => "The happy dad said happy stuff."
# add_more_ruby("Sad times are ahead!")         # => "Happy times are ahead!"

def add_more_ruby(string)
  string.gsub(/[sS]ad/, 'sad' => 'happy', 'Sad' => 'Happy')
end

# I was unsure how to check for a case-sensitive word.  I got my answer from Stack Overflow,
# and admittedly I don't fully understand the answer.  Going to go over it.





