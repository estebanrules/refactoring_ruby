# A grad student at a local university thinks he has discovered a formula to
# predict what kind of grades a person will get. He says if you own less than
# 10 books, you will get a "D". If you own 10 to 20 books, you will get a "C",
# and if you own more than 20 books, you will get a "B".
# He further hypothesizes that if you actually read your books, then you will
# get a full letter grade higher in every case.
#
# grade(4,  false)  # => "D"
# grade(4,  true)   # => "C"
# grade(15, true)   # => "B"

def grade(num_books, reads_books)
  if num_books < 10 and reads_books == false
    "D"
  elsif num_books < 10 and reads_books == true
    "C"
  elsif num_books.between?(10, 20) and reads_books == false
    "C"
  elsif num_books.between?(10, 20) and reads_books == true
    "B"
  elsif num_books > 20 and reads_books == false
    "B"
  elsif num_books > 20 and reads_books == true
    "A"
  end
end

# Notes:
#
# This was their solution:
#
#  def grade(num_books, reads_books)
#    if reads_books
#      return "C" if num_books <  10
#      return "B" if num_books <= 20
#      return "A"
#    else
#      return "D" if num_books <  10
#      return "C" if num_books <= 20
#      return "B"
#    end
#  end
#
#  Whis is much better and follows the basic principle of DRY.
#
#  To Vic:  In their solution, they were able to use the following code:
#
#  if reads_books (with out a == true/false)
#
#  Because reads_books == true by default?
