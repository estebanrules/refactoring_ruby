# Replace 'Batman' with 'Superman' in a given string.
def replace_string(str)
  str.sub('Batman', 'Superman')
end

str = "Batman is cool and Batman is fast."

puts replace_string(str)

# Wait a second...that only replaces the FIRST instance of 'Batman' in the string.
# Not only is Superman cool, but he is also fast.  Let's fix that.
def replace_string_global(str)
  str.gsub('Batman', 'Superman')
end

puts replace_string_global(str)
