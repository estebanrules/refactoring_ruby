# Replace all vowels with the number 1
def replace_vowels_with_integers(str)
  str.gsub(/[aeiou]/,'1')
end

str = "Ruby is far superior to Python."

puts replace_vowels_with_integers(str)

# Replace all capital letters with the number 0.
def replace_caps_with_integers(str)
  str.gsub(/[aeiou]/,'0')
end

puts replace_caps_with_integers(str)
