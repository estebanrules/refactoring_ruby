# Basic SQLite / ActiveRecord Tutorial
# First, we load the active_record/sqlite module
require 'active_record'

# Here we specify a destination to save our log.
# We could also send the log directly to the terminal using:
# Logger.new(STDERR)
ActiveRecord::Base.logger = Logger.new(File.open('database.log', 'w'))

# Finally, we actually connect to the SQLite DB.  We could also store
# data all in memory (though it wouldn't persist) via:
# :database => ':memory'
# OMG!  It's just a hash! Rails really IS magic!
ActiveRecord::Base.establish_connection(
  :adapter => 'sqlite3',
  :database => 'example.db'
)

# So we need to define a schema.  A Schema helps dictate in code what the structure
# of the database should be and what the different columns/rows and data types should be used.
# OMG, it's a loop and a block! Rails really IS magic!
ActiveRecord::Schema.define do
  unless ActiveRecord::Base.connection.tables.include? 'albums'
    create_table :albums do |table|
      table.column :title,       :string
      table.column :performer,   :string
    end
  end

  unless ActiveRecord::Base.connection.tables.include? 'tracks'
    create_table :tracks do |table|
      table.column :album_id,      :integer # foreign key <table-name-singular>_id
                                            # (i.e., this is the primary key from the
                                            # 'albums' table)
      table.column :track_number,  :integer
      table.column :title,         :string
    end
  end
end
# So what did we do above?
#
# 1 - We're defining a new Schema.
# 2 - Second, we're creating two tables: albums and tracks (which we wrap the creation of in a
# conditional statement so we make sure we don't cause any errors by trying to create a table
# that already exist.)  FYI, the convention is to name the tables as plurals (notice we don't call it
# it album or track - as they'll be holding multiples of that data)
# 3 - Thirdly, inside of the table creation we're specifying columns and what
# their data types should be.
#
# When we create a table a "primary key" is automatically created for us and is named after
# the table.  So in this instance we have two primary keys created for us:
# albums_id and tracks_id (again, notice the naming convention of tableName_id - singular,
# not plural)
#
# Inside the tracks table you'll see the first column we create is actually a "foreign key"
# because we're creating a column which is named after the 'albums' table's primary key (album_id)

# Creating
# The ActiveRecord pattern is based on conventions, so in this instance we'll create
#  two new classes that inherit from ActiveRecord's base class and we'll
#  use these two classes for creating new records for each table...
class Album < ActiveRecord::Base
  has_many :tracks
end

class Track < ActiveRecord::Base
  belongs_to :album
end

unless Album.find_by_title('In Utero')
  album = Album.create(
    :title => 'In Utero',
    :performer => 'Nirvana'
  )
  # an array!  Rails really IS...you get it.
  track_listing = [
    nil,
    'Serve the Servants',
    'Scentless Blah',
    'Crappy Nirvana Song #1',
    'Crappy Nirvana Song #2',
    'Blah, blah',
    'Wah, Wah'
  ]
  # Enumerable
  track_listing.each_with_index do |value, index|
    album.tracks.create(:track_number => index, :title => value) unless index === 0 # skip zero index
  end
end

unless Album.find_by_title('Zeppelin IV')
  album = Album.create(
    :title => 'Zeppelin IV',
    :performer => 'Led Zeppelin'
  )

  track_listing = [
    nil,
    'Misty Mountain Hop',
    'No Quarter',
    'Hobbit Song',
    'Orc Song',
    'Sauron baby, please treat me right'
  ]

  # Enumerable
  track_listing.each_with_index do |value, index|
    album.tracks.create(:track_number => index, :title => value) unless index === 0 # skip zero index
  end
end
# A few things:
# 1 - When we inherit from ActiveRecord::Base our class is mapped to a table of
# the same name.  So, for example, our class Album is mapped to the 'albums' table.
# 2 - Also, as we are inheriting from the ActiveRecord Base class, we don't need to
# specify attributes (for example, inside our Tracks class) such as :title or
# :track_number within our class, as they will be indirectly inferred from the Schema
# we defined earlier and will come from that table which the class is mapped to.

# Association
# 1 - You'll notice within the Album class we call a 'has_many' mehotd (provided through the inheritance
# chain via ActiveRecord::Base) which sets up the association between 'albums' and 'tracks'.
# 2 - Similarly, within the Track class we call the 'belongs_to' and tell it that our tracks belong to the
# 'albums' table.

# Record Creation
# We call a create method like so...
# album = Album.create(...)
# which is the same as...
# album = Album.new(...);
# album.save

# when we create a new Album instance we can access and create new Tracks as well
# (via the album instance: album.tracks.create(...)).  This is because we have made
# an association within the top level classes between Albums and Tracks.

# To create records, we could do this:
album.tracks.create(:track_number => 1, :title => 'a')
album.tracks.create(:track_number => 2, :title => 'b')
album.tracks.create(:track_number => 3, :title => 'c')

# But much better to create the track listing as an Array and then loop the Array creating new records.

# We use this bit of code to avoid including the [0] item in the Array:
# album.tracks.create(:track_number => index, :title => value) unless index === 0

begin
  p Album.find(1).tracks.length

rescue ActiveRecord::RecordNotFound
  p 'We just rescued a "RecordNotFound" error'
end

