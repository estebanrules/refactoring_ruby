# Let's generate some XML!
require 'builder'

xml = Builder::XmlMarkup.new(target: STDOUT, indent: 2)
xml.super_hero(company: "Marvel") do
  xml.name do
    xml.real_name("Logan")
    xml.alias("Wolverine")
  end
  xml.species("Mutant")
  xml.super_powers("Healing Factor, Adamantium Skeleton, Claws")
end

# Output:
# <super_hero company="Marvel">
#   <name>
#     <real_name>Logan</real_name>
#     <alias>Wolverine</alias>
#   </name>
#   <species>Mutant</species>
#   <super_powers>Healing Factor, Adamantium Skeleton, Claws</super_powers>
# </super_hero>
# 
