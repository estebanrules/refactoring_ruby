# Simple example of OpenStruct
# An open structure is an object who attributes are created dynamically
# when first assigned.  If obj is an instance of OpenStruct, then the
# statement obj.abc = 1 will create the attribute abc in obj and then
# assign the value 1 to it.
require 'ostruct'

floyd_albums = OpenStruct.new("album1" => "Dark Side of The Moon",
                         :album2 => "Wish You Were Here")

floyd_albums.album3 = "Animals"
floyd_albums.album4 = "The Wall"

puts floyd_albums.album1
puts floyd_albums.album2
puts floyd_albums.album3
puts floyd_albums.album4
p floyd_albums

# But how does this "magic" work?  OpenStruct uses method_missing to
# intercept calls.  This could cause a problem, because calls to a method
# defined in class Object will not invoke method_missing, they'll simply
# call the method in Object.
# Below is a typical example:
ice = OpenStruct.new
ice.freeze = "yes"
p ice.freeze # This produces: #<OpenStruct freeze="yes">

# BUT, if you don't call the setter, the freeze getter will not invoke
# method_missing - it will simply call the underlying freeze method in Object.
icey = OpenStruct.new
p ice.freeze
ice.freeze = "yes" # 1_openstruct.rb:33:in `<main>': can't
                   # modify frozen OpenStruct (RuntimeError)
