# Here is a method that measures the execution time
# of any block passed to it.
def measure_time
  start = Time.now
  yield
  puts "Completion time: #{Time.now - start} seconds."
end

# Call it this way:
measure_time do
  sleep 3
end

# output:
# Completion time: 3.000171153 seconds.

# or
measure_time {sleep 3}

# output:
# Completion time: 3.000171153 seconds.
