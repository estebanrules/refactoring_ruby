# Blocks are really just a syntactical construct
# of putting do/end or {} after a method invocation.
# When you write code with this syntactical construction,
# Ruby arranges for the code in the block to be passed to
# a method being called.
# So, again:
def say_something
  yield # yield execution to the block
end

# Call the method with a block
say_something { puts "Hello, Blocks!" }

# The above is the same as this:
say_something do
  puts 'Hello again!'
end

# As we already know, if we called the method say_something
# without a block we'll get a LocalJumpError exception.
# But...there is a "safe way".
def say_something_safe
  yield if block_given?
end

# Method that expects a block that takes an argument:
def say_something_to_friend
  # Argument 'my friend' passed to the block
  yield('my friend!')
end

# Call the method with a block that takes an argument:
say_something_to_friend { |arg| puts "Hello, #{arg}" }
# Output:
# Hello, my friend!

# Which is again the same as doing the following:
say_something_to_friend do |arg|
  puts "Hello, #{arg}"
end

# To Vic: I knew that do/end statements were blocks, but
# seeing the comparison of the usage of do/end and {}
# is helpful.  I recall a section in the style guide
# about when to use do/end vs. {}.  Must consult.
