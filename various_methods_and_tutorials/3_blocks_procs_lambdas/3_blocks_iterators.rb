# Another common use of blocks is in conjunction with Ruby iterator methods.
# Example 1:
[1, 2, 3, 4, 5].each { |elem| puts "#{elem * 2}" }

# output:
# 2
# 4
# 6
# 8
# 10

# Example 2:  each_with_index. key, value and index of the elements
# in a hash are passed to the block.
hsh = {:a => 30, :b => 31, :c => 32}

hsh.each_with_index do |(k, v), index|
  puts "#{index}.  #{k} => #{v}"
end

# Output:
# 0.  a => 30
# 1.  b => 31
# 2.  c => 32
