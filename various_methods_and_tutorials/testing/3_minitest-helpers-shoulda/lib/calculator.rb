# Simple Calculator
class Calculator
  def add(num1, num2)
    num1 + num2
  end

  def subtract(num1, num2)
    num1 - num2
  end

  def multiply(num1, num2)
    num1 * num2
  end

  def divide(num1, num2)
    num1 / num2
  end

  def calculate_remainder(num1, num2)
    num1 % num2
  end

  def compare(num1, num2)
    num1 == num2
  end

  def greater_or_less_than(num1, num2)
    num1 > num2
  end
end
