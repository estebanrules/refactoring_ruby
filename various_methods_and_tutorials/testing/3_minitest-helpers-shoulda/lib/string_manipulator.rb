# Let's test some string functions!
class StringManipulator
  def reverse_string(string)
    string.reverse!
  end

  def capitalize_string(string)
    string.capitalize!
  end

  def string_to_array(string)
    string.split(" ")
  end

  def upcase_string(string)
    string.upcase!
  end
end
