# file: test/test_calculator.rb
# With shoulda, our tests look more "ruby-esque"
# These tests are great, but the output isn't as nice as RSpec or Cucumber.
# Let's fix that.
# look in test_helper.rb to see changes.
# Added more tests.
# TODO: Add more tests!
require 'test_helper'
require_relative '../lib/string_manipulator'

class TestStringManipulator < Minitest::Test
  context 'a string_manipulator' do
    setup do # notice the difference?
      @str_man = StringManipulator.new
    end

    should 'reverse a string properly' do
      assert_equal "esrever", @str_man.reverse_string("reverse")
    end

    should 'not reverse a string incorrectly' do
      refute_equal "reverse", @str_man.reverse_string("reverse")
    end

    should 'capitalize a string properly' do
      assert_equal "Big", @str_man.capitalize_string("big")
    end

    should 'split a string into an array properly' do
      assert_equal ["Hello", "Vic"], @str_man.string_to_array("Hello Vic")
    end

    should 'convert a string into all uppercase characters properly' do
      assert_equal "UP", @str_man.upcase_string("up")
    end
  end
end



