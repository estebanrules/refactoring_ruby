# file: test/test_calculator.rb
# With shoulda, our tests look more "ruby-esque"
# These tests are great, but the output isn't as nice as RSpec or Cucumber.
# Let's fix that.
# look in test_helper.rb to see changes.
# Added more tests.
# TODO: Add more tests!
require 'test_helper'
require_relative '../lib/calculator'

class TestCalculator < Minitest::Test
  context 'a calculator' do
    setup do # notice the difference?
      @calc = Calculator.new
    end

    should 'add two numbers properly' do
      assert_equal 4, @calc.add(2, 2)
    end

    should 'not add incorrectly' do
      refute_equal 5, @calc.add(2, 2)
    end

    should 'subtract two numbers properly' do
      assert_equal 0, @calc.subtract(2, 2)
    end

    should 'not subtract incorrectly' do
      refute_equal -1, @calc.subtract(2, 2)
    end

    should 'multiply two numbers properly' do
      assert_equal 4, @calc.multiply(2, 2)
    end

    should 'not multiply incorrectly' do
      refute_equal 3, @calc.multiply(2, 2)
    end

    should 'divide two numbers properly' do
      assert_equal 3, @calc.divide(6, 2)
    end

    should 'not divide incorrectly' do
      refute_equal 2, @calc.divide(6, 2)
    end

    should 'calculate the remainder of two numbers properly' do
      assert_equal 1, @calc.calculate_remainder(7, 2)
    end

    should 'not calculate the remainder of two numbers incorrectly' do
      refute_equal 3, @calc.calculate_remainder(7, 2)
    end

    should 'correctly determine if two numbers are equal' do
      assert_equal true, @calc.compare(2, 2)
    end

    should 'not incorrectly determine if two numbers are equal' do
      refute_equal true, @calc.compare(2, 3)
    end

    should 'correctly determine if two numbers are not equal' do
      assert_equal false, @calc.compare(2, 3)
    end

    should 'not incorrectly determine if two numbers are not equal' do
      refute_equal false, @calc.compare(2, 2)
    end

    should 'correctly determine if one number is greater than the other' do
      assert_equal true, @calc.greater_or_less_than(3, 2)
    end

    # should 'not incorrectly determine if one number is greater than the other' do
    #  refute_equal true, @calc.greater_or_less_than(2, 3)
    #end
  end
end
