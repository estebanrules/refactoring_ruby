# file: test/test_helper.rb
# Notice how we don't 'require calculator', this is so we can eventually test
# beyond the Calculator class.  Not everything in our library will need to
# include 'calculator', so we do not include it in our test_helper.rb file.
require 'minitest/autorun'
require 'minitest/reporters'
require 'shoulda/context'

# Let's add the line below for spec-like progress
Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new
