# Ruby 2.0 blocks can also be defined with keywords arguments
define_method(:foo) do |bar: 'default'|
  puts bar
end

foo # => 'default'
foo(bar: 'baz')

# Note: Defining blocks with keyword arguments?  Interesting.
