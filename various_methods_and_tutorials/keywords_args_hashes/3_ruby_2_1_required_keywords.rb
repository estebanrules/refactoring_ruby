# Ruby 2.1 introduced REQUIRED keyword arguments
# which are defined with a trailing colon:
def foo(bar:)
  puts bar
end

foo # => ArgumentError: missing keyword: bar
foo(bar: 'baz') # => 'baz'

# Without the trailing colon, Ruby will raise an
# ArgumentError that tells us which required argument
# we must include.
