# We have this method with positional arguments:
def mysterious_total(subtotal, tax, discount)
  subtotal + tax - discount
end

mysterious_total(100, 10, 5) # => 105

# The above method does its job, we as a reader of the method,
# we have no idea what those arguments mean without looking up
# the implementation of the method.
# BUT, by using keyword arguments, we know what the arguments mean without
# looking up the implementation of the called method:
def obvious_total(subtotal:, tax:, discount:)
  subtotal + tax - discount
end

obvious_total(subtotal: 100, tax: 10, discount: 5) # => 105

# Isn't that nicer?
# Keyword arguments also allow us to switch the order of the arguments:
obvious_total(subtotal: 100, discount: 5, tax: 10) # => 105

# With positional arguments we can't do this:
mysterious_total(100, 5, 10) # => 95 # The customer got a larger discount!
