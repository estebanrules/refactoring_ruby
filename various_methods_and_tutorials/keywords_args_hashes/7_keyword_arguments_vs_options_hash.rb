**With first-class keyword arguments in the language, we don't have to
write the boilerplate code to extract hash options (whoo hoo!).**

With keyword arguments defined in the method signature itself, we can
immediately discover the names of the arguments without having to read
the body of the method.

**The "calling code" is syntactically equal to calling a method
with hash arguments!**
