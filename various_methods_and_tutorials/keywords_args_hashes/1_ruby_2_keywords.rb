# Ruby 2.0 introduced first-class support for keyword arguments:
def foo(bar: 'default')
  puts bar
end

foo # => 'default

foo(bar: 'baz') # => 'baz'

# In Ruby 1.9, we could do something similar with a single Hash parameter:
def foo_original(options = {})
  bar = options.fetch(:bar, 'default')
  puts bar
end

foo_original # => 'default'

foo_original(bar: 'baz') # => 'baz'




