# Just playing around.
class ComicHash
  def initialize(superheroes: [], super_villains: [])
    @superheroes    = superheroes
    @super_villains = super_villains
  end
end

comic_hash = ComicHash.new(superheroes: ['Batman', 'Superman', 'The Flash'],
                           super_villains: ['Bizarro', 'Ras al Ghul',
                                            'Captain Cold'])

p comic_hash


