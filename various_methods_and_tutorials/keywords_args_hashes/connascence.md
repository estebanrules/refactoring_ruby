## Connascence and trade-offs:

**READ:** *What Every Programmer Should Know About Object-Oriented Design*
by Meiler Page-Jones and *Ruby Best Practices Issue #24*

**WATCH:** Jim Weirich's talk "Connascence Examined"
