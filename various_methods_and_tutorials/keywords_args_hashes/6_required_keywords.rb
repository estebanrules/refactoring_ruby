# Playing around with required keywords.

class KeywordHash
  include Enumerable

  def initialize(jla: [], avengers: [], teen_titans: [])
    @jla = jla
    @avengers = avengers
    @teen_titans = teen_titans
  end

  def each
    yield "jla : #{@jla}"
    yield "avengers: #{@avengers}"
    yield "teen_titans: #{@teen_titans}"
  end
end

super_hash = KeywordHash.new(jla: ['Superman', 'Batman', 'Green Lantern'],
                             avengers: ['Thor', 'Iron Man', 'Captain America'],
                             teen_titans: ['Robin', 'Kid Flash', 'Wonder Girl'])

p super_hash

super_hash.each { |k, v| puts k, v }
