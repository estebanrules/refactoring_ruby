# Outputs a Fibonacci sequence, the length of the sequence being determined by the user.
puts "How many numbers from the sequence would you like to see?"
a = gets.chomp.to_i

def fib(n)
  return n if (0..1).include? n
  fib(n-1) + fib(n-2) if n > 1
end

a.times do |i|
  puts fib(i)
end
