# The ternary operator.
# The definition of ternary - "composed of three items."
# Basic shorthand for if-then-else statement.
def check_sign(number)
  number > 0 ? "#{number} is positive" : "#{number} is negative"
end

puts check_sign(12)
