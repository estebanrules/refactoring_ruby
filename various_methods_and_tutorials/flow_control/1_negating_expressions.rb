# Some basic flow control
def check_name(name)
  name != "Bob"
end

name = "Mike"
puts check_name(name)

name = "Bob"
puts check_name(name)

def check_age(age)
  age >= 65
end

age = 66
puts check_age(age)

age = 32
puts check_age(age)

# To Vic:
# These simple examples are for the purpose of learning
# to write tests.  Would you recommend MiniTest?

