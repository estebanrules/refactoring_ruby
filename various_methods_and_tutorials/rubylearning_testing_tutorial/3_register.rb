# Testing with MiniTest continued...
require 'minitest/autorun'
require 'minitest/unit'
require 'minitest/pride'

# Our Tests.
class TestCashRegister < MiniTest::Test
  def setup
    @register = CashRegister.new
  end

  def test_default_is_zero
    assert_equal 0, @register.total
  end
  # We want to scan two things in, and make sure that the total is correct.
  def test_total_calculation
    @register.scan 1
    @register.scan 2
    assert_equal 3, @register.total
  end
end

# Ok.  So we need to have the scan method the price to a list in order to
# return the sum.  We add an array to the initialize method for CashRegister
# that we can add prices to the array and get the sum.
# We also how to modify our total method like so:

class CashRegister
  def initialize
    @items = []
  end

  def total
    @items.inject(0) { |sum, item| sum += item } # or @items.inject(0, &:+) GO OVER THIS
  end

  def scan(item)
    @items << item # this method let's us add to our item array so that we can compute the total of the items.
  end
end

# And with that, we're in the green, baby!  The green!
