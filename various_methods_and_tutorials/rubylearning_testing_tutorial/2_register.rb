# Testing with minitest continued.
# 1 - The register will have a scan method that takes in a price, and records it.
# 2 - The register will have a total method that shows the current total of all
#      the prices that have been scanned so far.
# 3 - If no prices have been scanned, the total should be zero.
# 4 - The register will have a clear method that clears the register
#     of all scanned items. The total should go back to zero again.
# So far, we have fulfilled objective 3.

require 'minitest/autorun'
require 'minitest/unit'
require 'minitest/pride'

# We add some more code to our test.
# Objective 1 says that this method should be called scan, so let's write a test.
# We'll put it in our test class with the test_default_is_zero method.
class TestCashRegister < MiniTest::Test
  def setup
    @register = CashRegister.new
  end

  def test_default_is_zero
    assert_equal 0, @register.total
  end
  # We want to scan two things in, and make sure that the total is correct.
  def test_total_calculation
    @register.scan 1
    @register.scan 2
    assert_equal 3, @register.total
  end
end
# We run the above with no scan method, we get one error.  Let's add a scan method.
# We add a scan method, and get a 'wrong number of arguments (1 for 0)' error.
# Let's make it so it takes arguments.
# After we add an argument (price) it gives uf a failure, Expected: 3, Actual: 0.
# In our test we wrote that it would return 3, our current method returns nil (0).
class CashRegister
  def initialize
    @items = []
  end

  def total
    0
  end

  def scan(item)

  end
end
# See part three.

