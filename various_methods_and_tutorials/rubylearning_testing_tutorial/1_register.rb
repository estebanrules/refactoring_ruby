# Rubylearning - How do I test with Minitest?
# Let's do somethign simple. so we can focus on the tests.
# We'll make a Ruby class called CashRegister.
# It'll have several features, but here's the first two we need:
# 1 - The register will have a scan method that takes in a price, and records it.
# 2 - The register will have a total method that shows the current total of all the prices
#     that have been scanned so far.
# 3 - If no prices have been scanned, the total should be zero.
# 4 - The register will have a clear method that clears the register
#     of all scanned items. The total should go back to zero again.
require 'minitest/autorun'
require 'minitest/unit'
require 'minitest/pride'

class TestCashRegister < MiniTest::Unit::TestCase
  def setup
    @register = CashRegister.new
  end

  def test_default_is_zero
    assert_equal 0, @register.total
  end
end
# Let's take it line  by line.
# 1 - On the first line, we have 'require', the autorun part of
#     minispec includes everything you need to run your tests, automatically.  All we need
#     to do to run our tests is to type 'ruby register.rb'
# 2 - Next we set up a class that inherits from one of minitest's base classes.
#     That's how minitest works, by running a series of TestCases.  It also lets you
#     group similar tests together, and split different ones up into multiple files.
# In this class, we have two methods: the first is the setup method
# This runs before each test, and allows us to prepare for the test we want to run
# In this case, we want a new CashRegister each time, and we’ll store it in a variable
# Now we don’t have to repeat our setup over and over again… it’s just automatic!
#
# Finally, we get down to business, with the test_default_is_zero method.
# Minitest will run any method that starts with test_ as a test.
# In that method, we use the assert_equal method with two arguments.
# assert_equal is where it all happens, by comparing 0 to our register’s total,
# and it will complain if they’re not equal.

# When we run the test with nove code, we get an error that there is no class CashRegister
# So...
class CashRegister
  def total
    0
  end
end
# After we run the above code we see progress, we get an error saying there is no class
# method 'total' (as we have specified in our tests.)

# We run it again after adding the blank total method.  The method returns nil,
# but the test expects it to return 0, so we get 1 failure.

# After we add the zero, it's all good.  We have
# fulfilled objective 3, and of the test we've written so far.
# # See part 2.
#
