# Simple example of a code block with yield.
def which_superteam
  yield("Captain America", "Avengers")
  yield("Superman", "Justice League")
end

which_superteam { |hero, team| puts "#{hero} is a member of The #{team}" }
