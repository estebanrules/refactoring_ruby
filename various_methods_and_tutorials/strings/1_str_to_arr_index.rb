# Split a string in to an array, print out
# the values and their index number like so:
# value : index

str = "abcdefg"

str.split('').each_with_index { |x, i| puts "#{x} : #{i}" }

# Output:
# a : 0
# b : 1
# c : 2
# d : 3
# e : 4
# f : 5
# g : 6

# Split a string in to an array, print out
# the values and their index number like so:
# index : value

str.split('').each_with_index { |x, i| puts "#{x} : #{i}".reverse }

# Output:
# 0 : a
# 1 : b
# 2 : c
# 3 : d
# 4 : e
# 5 : f
# 6 : g


