# Compute the average of numbers in an array
def average(numbers)
  numbers.inject(:+) / numbers.length
end

puts average([1, 2, 3, 4, 5])

# The above method works great.  But what if there is nothing in the array?
# The inject method passes an accumulator object (called memo) AND the item
# itself in to the block, or invokes memo.send(sym, obj).  At each step,
# memo is set to the value returned by the block on the previous step.
# This form lets you specify an initial value for memo:
def average2(numbers)
  numbers.inject(0, :+) / numbers.length
end

puts average2([0])

# Or you could do this:
def average3(numbers)
  numbers.inject(0) { |sum, element| sum + element } / numbers.length
end

puts average3([1, 2, 3, 4, 5])
