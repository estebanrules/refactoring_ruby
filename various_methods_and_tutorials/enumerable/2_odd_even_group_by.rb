# Write a method that takes an array and divides
# the values in to two hashes, "odd" or "even".
def odd_or_even(numbers)
  numbers.group_by { |num| num.even? ? "even" : "odd" } # ternary operator
end

puts odd_or_even([4, 8, 1, 2, 12, 14, 11, 19, 21])

