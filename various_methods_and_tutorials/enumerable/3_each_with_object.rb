# Enumerable#each_with_object is similar to
# Enumerable#inject, yet oh so slightly different.
# From the docs:
#
# Enumerable#each_with_object -
# Iterates the given block for each element with an
# arbitrary object given, and returns the initially
# given object.
#
# Enumerable#inject -
# Iterates over a collection, passing the current element and
# the memo to the block. Handy for building up hashes or reducing
# collections down to one object.

# Pass a range in to a block, multiply
# each number in the range by two and return the multiplied
# numbers as an array.
p evens = (1..10).each_with_object([]) {|i, a| a << i*2}

# Pass an array containing lowercase strings in to a block,
# return a hash with the original strings as keys and the
# capitalized strings as values.
p %w(ruby smalltalk lisp haskell).each_with_object({}) { |str, hsh| hsh[str] = str.upcase }

