# As mentioned in the previous example,
# Enumerable#each_with_object and Enumerable#inject
# are (kind of) similar.

# Example:  modifying both the keys and values of a hash via inject.
hsh_one = {:kay => "smalltalk", :matz => "ruby"}
p hsh_one.inject({}) { |h, (k, v)| h[k.upcase] = v.upcase; h }

# Example:  modifying both the keys and values of a hash via each_with_object.
hsh_two = {:ritchie => "c", :mccarthy => "lisp"}
p hsh_two.each_with_object({}) { |(k, v), h| h[k.upcase] = v.upcase }

# Notes for Vic:
# I'm getting a fairly decent grasp on these types of methods.
# Is the syntax of the first example (|h, (k, v)|) necessary because
# of memo?
