# Lambdas are similar to Procs.  An example:
def test_lambda(some_lambda)
  some_lambda.call
end

some_new_lambda = -> {puts "in the lambda"}
test_lambda(some_new_lambda)
# Output:
# in the lambda

# So how are lambdas and Procs different?
# 1 - Lambdas check the number of parameters passed into the call,
# Procs do not.
# 2 - The way in which they handle return.  Look at the secon example.

# Example 1:
def test_parameter_handling(code)
  code.call(1, 2)
end

l = lambda {|a, b, c| puts "#{a} is #{a.class}, #{b} is a #{b.class} and #{c} is a #{c.class}"}
p = Proc.new {|a, b, c| puts "#{a} is #{a.class}, #{b} is a #{b.class} and #{c} is a #{c.class}"}

test_parameter_handling(p)
test_parameter_handling(l)
# Output:
# 1 is Fixnum, 2 is a Fixnum and  is a NilClass
# 3_lambdas.rb:21:in `block in <main>': wrong number of arguments (2 for 3) (ArgumentError)

# See?  The Proc doesn't care if we only call with two parameters when it's
# possible to use three parameters.  It simply sets all unused parameters to nil.
# However, when passing a lambda to a method, Ruby throws an ArgumentError instead.

# Example 2 (how they handle the return statement differently):
def return_using_proc
  Proc.new { return "Hi from proc!" }.call
  puts "end of proc call."
end

def return_from_lambda
  -> { return "Hi from lambda!" }.call
  puts "end of lambda call."
end

puts return_from_proc
puts return_from_lambda
