# Ruby Closures
# Procs, lambdas and blocks are all closures.  Basically, this means that
# they hold the values of any variables that were around when they were
# created.  This is a very powerful feature of Ruby, if we wrap a block
# of code in a method call we can dynamically create different behavior:
def create_multiplier(m)
  lambda { |val| val * m }
end

two_times = create_multiplier(2)
three_times = create_multiplier(3)

puts two_times.call(2)
puts three_times.call(2)

# To Vic:
# In order to fully grasp Procs / lambdas, I think I need
# to make something from scratch (as is often the case when
# learning something new).
# Any recommendations for a potential project that employs
# heavy usage of Procs and/or lambdas?

