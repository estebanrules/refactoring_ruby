# Procs.
# It is possible to get a handle on the block by explicitly
# putting the block as a parameter in the method definition.
# This must be the last parameter and start with an ampersand.
def test_blocks(&block)
  puts block.class
end

test_blocks { puts "in the block" }
# Output:
# Proc
# Notice that the class is Proc!  The only difference between a Proc
# and a block is that a Proc can be passed around as an explicit variable.
# Below is an example where we create a Proc object first and pass that into
# another method that expects a Proc.
def test_blocks2(some_proc)
  puts some_proc.call
end

some_new_proc = Proc.new { puts "in the Proc!" }
test_blocks2(some_new_proc)
# Output
# in the Proc!

