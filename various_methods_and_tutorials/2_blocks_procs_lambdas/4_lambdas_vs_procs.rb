# Example 2 (how they handle the return statement differently):
def return_using_proc
  Proc.new { return "Hi from proc!" }.call
  puts "end of proc call."
end

def return_using_lambda
  -> { return "Hi from lambda!" }.call
  puts "end of lambda call."
end

puts return_using_proc
puts return_using_lambda
# Output:
# Hi from proc!
# end of lambda call.

# Weird.  When return_using_proc is called, the method stops processing as soon
# as it encounters the return statement, and we see "Hi from proc!" output.
# BUT, when return_using_lambda is called, the return is encountered but
# does not cause the method to return and instead the method continues to run
# so we see "end of lambda call" output.

