# More blocks.
# Blocks are code that can be implicitly passed to a method in Ruby.
# ANY method can in fact be passed a block of code even if the method
# does not explicitly expect it.
# Example:
def test_blocks
  puts "in test_blocks"
end

test_blocks { puts "in the block" }
# output: in test_blocks

# In the above example, the method was passed the block but wasn't "expecting"
# it, thus the method simply printed out "in test_blocks".
# But, if we add yield:
def test_blocks2
  puts "in test_blocks2"
  yield
end

test_blocks2 { puts "in the block" }
# output:
# in test_blocks2
# in the block
# Due to the yield statement, the block was passed in to the method and
# executed.

