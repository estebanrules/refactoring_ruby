# Simple example of a code block with yield.
require 'pp'
def which_superteam
  yield("Captain America", "Avengers")
  yield("Superman", "Justice League")
end

which_superteam { |hero, team| puts "#{hero} is a member of The #{team}" }

# A few example of looping / iteration with code blocks.
['Wonder Woman', 'Aquaman', 'Batman'].each { |hero| puts hero }

# The each method only iterates over the Array.  What if we wanted to make
# a new Array with the results of our block?  collect.
superhero_collection = ['Wonder Woman', 'Aquaman', 'Batman'].collect{ |hero| "#{hero} is a hero" }
pp superhero_collection

# Let's print a bunch of stars.
80.times { print "*" }

# Let's print a range of integers(with spaces between)
1.upto(40) { |i| print i, " " }

# Let's print a range of capital letters.
('A'..'F').each { |char| print char }


