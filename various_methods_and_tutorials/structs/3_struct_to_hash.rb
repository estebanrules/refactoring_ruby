# TODO: Write examples of these classes.

# Struct to Hash:
class Struct
  def to_hash
    Hash[*members.zip(values).flatten]
  end
end

# Hash to Struct
class Hash
  def to_struct(name)
    Struct.new(name, *keys).new(*values)
  end
end
