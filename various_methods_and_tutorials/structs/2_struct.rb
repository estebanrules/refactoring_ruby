# Structs continued.
# What if we want to change the to_s method of our Struct?
class SuperHero < Struct.new(:name, :super_power, :alignment)
  def to_s
    "#{self.name}'s powers are #{self.super_power} and his alignment is #{self.alignment}."
  end
end

Vic = SuperHero.new('Vic', 'Coding and uber Sysops/Devops skills', 'chaotic/neutral')

puts Vic

# To Vic:
# Feel free to change the attributes ;).
# On a semi-serious note, Structs are all well and good, but
# I don't really see their value.  I suppose for prototyping?  Would
# you use Structs in production?
