# From Programming Ruby 1.9, 2.0, 4th Edition (Pickaxe)
# Ruby comes with a class called Struct, which allows you to
# define classes that contain only data attributes.
# Example:
require 'pp'
SuperHero = Struct.new(:name, :super_power, :good_or_evil)

Superman = SuperHero.new('Clark Kent', 'Flight, Super Strength, Heat Vision, etc.')
Superman.good_or_evil = "good"

pp Superman


