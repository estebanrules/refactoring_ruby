# The Hash::[] class method can be used in addition
# to the splat operator to create a hash from an array.

arr = [:a, 1, :b, 2, :c, 3, :d, 4]
p Hash[*arr]
