# Let's write a class that can use operator expressions like built-in objects.
# from Programming Ruby 1.9, the "Pickaxe Book".
class ScoreKeeper
  def initialize
    @total_score = @count = 0 #NOTE:  I don't fully understand this assignment.
  end

  def <<(score) # Here we define the << method
    @total_score += score
    @count += 1
    self       # This method returns the value of self, as it is the
  end          # the last value in the method. (@total_score = @count = 0)

  def average
    fail "No scores" if @count.zero?
    Float(@total_score) / @count
  end
end

scores = ScoreKeeper.new
scores << 10 << 20 << 40
puts "Average = #{scores.average}"
