# Let's write a class with the [] method.
# from Programming Ruby 1.9, "the Pickaxe Book".
class SomeClass
  def []=(*params)
    value = params.pop
    puts "Indexed with #{params.join(', ')}"
    puts "value = #{value.inspect}"
  end
end

s = SomeClass
s[1] = 2
s['cat', 'dog'] = 'enemies'

# NOTE: Getting an undefined method []= error.  This code
# is exactly the same as in the Pickaxe Book.
