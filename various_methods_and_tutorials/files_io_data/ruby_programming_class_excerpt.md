Ruby is an object-oriented language in a very pure sense: every value in Ruby is (or at
least behaves like) an object. Every object is an instance of a class. A class defines a set
of methods that an object responds to. Classes may extend or subclass other classes,
and inherit or override the methods of their superclass. Classes can also include—or
inherit methods from—modules.
Ruby’s objects are strictly encapsulated: their state can be accessed only through the
methods they define. The instance variables manipulated by those methods cannot be
directly accessed from outside of the object. It is possible to define getter and setter
accessor methods that appear to access object state directly. These pairs of accessor
methods are known as attributes and are distinct from instance variables. The methods
defined by a class may have “public,” “protected,” or “private” visibility, which affects
how and where they may be invoked.
In contrast to the strict encapsulation of object state, Ruby’s classes are very open. Any
Ruby program can add methods to existing classes, and it is even possible to add
“singleton methods” to individual objects.
Much of Ruby’s OO architecture is part of the core language. Other parts, such as the
creation of attributes and the declaration of method visibility, are done with methods
rather than true language keywords. This chapter begins with an extended tutorial that
demonstrates how to define a class and add methods to it. This tutorial is followed by
sections on more advanced topics, including:

Method visibility
Subclassing and inheritance
Object creation and initialization
Modules, both as namespaces and as includable “mixins”
Singleton methods and the eigenclass
The method name resolution algorithm
The constant name resolution algorithm
7.1 Defining a Simple Class
We begin our coverage of classes with an extended tutorial that develops a class named
Point to represent a geometric point with X and Y coordinates. The subsections that
follow demonstrate how to:

Define a new class
Create instances of that class
Write an initializer method for the class
Add attribute accessor methods to the class
Define operators for the class
214 | Chapter 7: Classes and Modules• Define an iterator method and make the class Enumerable
• Override important Object methods such as to_s , == , hash , and <=>
• Define class methods, class variables, class instance variables, and constants
7.1.1 Creating the Class
Classes are created in Ruby with the class keyword:
class Point
end
Like most Ruby constructs, a class definition is delimited with an end . In addition to
defining a new class, the class keyword creates a new constant to refer to the class. The
class name and the constant name are the same, so all class names must begin with a
capital letter.
Within the body of a class , but outside of any instance methods defined by the class,
the self keyword refers to the class being defined.
Like most statements in Ruby, class is an expression. The value of a class expression
is the value of the last expression within the class body. Typically, the last expression
within a class is a def statement that defines a method. The value of a def statement is
always nil .
7.1.2 Instantiating a Point
Even though we haven’t put anything in our Point class yet, we can still instantiate it:
p = Point.new
The constant Point holds a class object that represents our new class. All class objects
have a method named new that creates a new instance.
We can’t do anything very interesting with the newly created Point object we’ve stored
in the local variable p , because we haven’t yet defined any methods for the class. We
can, however, ask the new object what kind of object it is:
p.class
# => Point
p.is_a? Point # => true
7.1.3 Initializing a Point
When we create new Point objects, we want to initialize them with two numbers that
represent their X and Y coordinates. In many object-oriented languages, this is done
with a “constructor.” In Ruby, it is done with an initialize method:
class Point
def initialize(x,y)
@x, @y = x, y
