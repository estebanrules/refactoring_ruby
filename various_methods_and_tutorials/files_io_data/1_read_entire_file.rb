require 'pp'

# Reading entire file:
data = IO.read("ruby_programming_class_excerpt.md")
pp data

# Read lines into an array:
words = IO.readlines("ruby_programming_class_excerpt.md")
pp words

# Readlines one at a time and initialize a hash:
words ={}
IO.foreach("ruby_programming_class_excerpt.md") { |w| words[w] = true }

pp words
