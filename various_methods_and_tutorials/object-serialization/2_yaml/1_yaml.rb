require 'yaml'

class ComicBook
  attr_accessor :title, :issue_number, :publisher

  def initialize(title, issue_number, publisher)
    @title = title
    @issue_number = issue_number
    @publisher = publisher
  end

  def to_yaml
    YAML.dump ({
      :title => @title,
      :issue_number => @issue_number,
      :publisher => @publisher
    })
  end

  def self.from_yaml(string)
    comic_info = YAML.load string
    p comic_info
    self.new(comic_info[:title], comic_info[:issue_number], comic_info[:publisher])
  end

  def to_json
    JSON.dump ({
      :title => @title,
      :issue_number => @issue_number,
      :publisher => @publisher
    })
  end

  def self.from_json(string)
    comic_info = JSON.load string
    self.new(comic_info['title'], comic_info['issue_number'], comic_info['publisher'])
  end
end

cb = ComicBook.new("Super Vic - Adventures in System V", 1, "Nix Comics")
p cb.to_yaml

cb = ComicBook.from_yaml(cb.to_yaml)
puts "Title: #{cb.title}"
puts "Issue Number: #{cb.issue_number}"
puts "Publisher: #{cb.publisher}"

# TODO: call JSON methods.
