# Saving YAML Data.
# Another handy little way of turning your objects into YAML
# format is provided by the dump method.  At its core, this converts your Ruby data
# into YAML format and "dumps" it into a string
# Example:
require "yaml"

names = ["Vic", "Superman", "Batman"]
yaml_names = YAML.dump(names)
p yaml_names

# Output:
# "---\n- Vic\n- Superman\n- Batman\n"

# Or (pay attention folks) the dump method can
# take a second argument, which is some kind of IO
# object, typically a file.
# Example:
f = File.open('names.yml', 'w')
YAML.dump(["Batman", "Superman", "Wonder Woman"], f)
f.close

File.foreach("names.yml") {|line| puts line}
# Output:
# ---
# - Batman
# - Superman
# - Wonder Woman

# OR, you can open a file and pass this into an associated block.
# Example:
File.open('more_names.yml', 'w'){|namesfile| YAML.dump(["Iron Man", "Thor", "Hulk"], namesfile)}

File.foreach("more_names.yml") {|line| puts line}

# To be continued...

