# YAML - YAML Ain't Markup Language
# YAML Basics
require "yaml"

# Convert an array to YAML
puts [1, 2, 3, 4].to_yaml
# Output:
# ---
# - 1
# - 2
# - 3
# - 4
# Notice the three dashes?  They define the start of a new YAML "document".
# In YAML terms, a document is not a separate file on disk but a separate
# YAML definition; one disk file may contain many YAML documents.

# Convert objects of non-standard types:
class Box
  def initialize(shape, color, size)
    @shape = shape
    @color = color
    @size = size
  end
end

puts box_one = Box.new("square", "blue", "8 X 8 X 8").to_yaml

# Output:
# --- !ruby/object:Box
# shape: square
# color: blue
# size: 8 X 8 X 8

# Another Example:
class CD
  def initialize(artist, album, genre)
    @artist = artist
    @album = album
    @genre = genre
  end
end

# side note:  I never thought about instantiating objects in this way, but why not?
# After all, it's just another object.
cd_collection = [CD.new("The Beatles", "The White Album", "Rock"),
                 CD.new("Rush", "2112", "Progressive Rock")]

puts cd_collection.to_yaml

# Output:
# ---
# - !ruby/object:CD
#   artist: The Beatles
#   album: The White Album
#   genre: Rock
# - !ruby/object:CD
#   artist: Rush
#   album: '2112'
#   genre: Progressive Rock

