# A YAML Database.
require "yaml"

class PinkFloydRecord
  def initialize(arr)
    @album_name = arr[0]
    @year_released = arr[2]
  end

  def get_details
    [@album_name, @year_released]
  end
end

record_one = ["Dark Side of The Moon", 1973]
record_two = ["Animals", 1977]

# To Vic:  Does this block contain proper syntax?  It looks a little, well, "non-rubyish".
# Most of the tutorials on YAML I have found are older.

# Write our objects to a yaml file.
File.open('floyd_record_db.yml', 'w') {|multiple_records|
  YAML.dump(record_one, multiple_records)
  YAML.dump(record_two, multiple_records)
  }

# Contents of floyd_record_db.yml:
# ---
# - Dark Side of The Moon
# - 1973
# ---
# - Animals
# - 1977

# Load our objects from a YAML file.
File.open('floyd_record_db.yml', 'r') {|multiple_records|
  YAML.load_documents(multiple_records) {|record|
  p record
  }
}
# Output:
# ["Dark Side of The Moon", 1973]
# ["Animals", 1977]

# To Vic:  This is obviously an elementary example of a YAML DataBase, the one
# that I will implement in the project will be more sophisticated.
# At this point do you recommend that I split up my program in to different files
# and do require_relative?
# Quick Update:  Included load_documents.
