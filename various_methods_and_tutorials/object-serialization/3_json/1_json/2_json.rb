require 'json'

# Let's read the serialized data from the file and reconstitute it.
serialized = File.read("data.json")
parsed_data = JSON.parse(serialized)

puts parsed_data
# Output:
# {:superhero=>"Aquaman", :super_powers=>["super-strength", "telekinesis", "other stuff"], :real_name=>"Arthur Curry"}


