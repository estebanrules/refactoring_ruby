require 'json'

# Serialize a data structure into a string and writes that to a file:
data = { superhero: 'Aquaman', super_powers: ['super-strength',
         'telekinesis', 'other stuff'], real_name: 'Arthur Curry' }

serialized = data.to_json

puts serialized

File.open("data.json", "w") { |f| f.puts serialized }
# Contents of file:
# {"superhero":"Aquaman","super_powers": ["super-strength","telekinesis","other stuff"], "real_name":"Arthur Curry"}

