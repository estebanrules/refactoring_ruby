require 'json'

# The methods j and jj convert their argument to JSON and write
# the result to STDOUT (jj prettyprints).
# Example:
data = { name: 'Bob', address: [ 'tx', 'usa'], age: 22 }

puts "Regular:"
j data

puts "\nPretty:"
jj data
# Output:
# Regular:
# {"name":"Bob","address":["tx","usa"],"age":22}

# Pretty:
# {
#   "name": "Bob",
#   "address": [
#     "tx",
#     "usa"
#   ],
#   "age": 22
# }
