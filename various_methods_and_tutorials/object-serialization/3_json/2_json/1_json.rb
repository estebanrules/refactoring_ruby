require 'json'
require 'pp'

json_read = JSON.load File.read("superman.json")
pp json_read

# output:
# {"superhero_name"=>"Superman",
#  "real_name"=>"Clark Kent",
#  "team_affiliation"=>"Justice League of America",
#  "team_members"=>
#   {"Batman"=>{"super_powers"=>"awesomeness"},
#    "Wonder Woman"=>
#     {"super_powers"=>"well-endowed, lasso of truth, flight, super-strength"},
#    "Aquaman"=>
#     {"super_powers"=>
#      "telekinetic link with marine life, super-strength, other"},
#    "Green Lantern"=>
#     {"super_powers"=>
#       "power ring that allows the user to transform their will into reality"}},
#  "super_powers"=>
#   ["super-strength", "flight", "heat vision", "everything else imaginable"]}

# Let's add this code to our ComicBook class, making ComicBook JSON-serializable
