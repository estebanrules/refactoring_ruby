# Sequel is based on ActiveRecord, but (may) have several
# advantages over it.
# Simple Sequel Example:
require "rubygems"
require "sequel"

DB = Sequel.sqlite

DB.create_table :author do
  primary_key :id
  String :name
end

authors = DB[:authors]
authors.insert(:name => 'Esteban')
