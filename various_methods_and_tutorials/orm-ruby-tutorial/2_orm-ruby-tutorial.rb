# DataMapper
# DM was developed to address perceived shortcomings in the ActiveRecord libraryand to create
# an ORM framework which is fast, thread-safe and feature-rich.
# Advantages:
# - No need to write migrations (NOTE: HUGE)
# - Respects the Single Responsibility Principle
# Simple Example:
require 'rubygems'
require 'dm-core'
require 'dm-migrations'

DataMapper::Logger.new($stdout, :debug)
DataMapper.setup(:default, 'sqlite://localhost/test')

class Author
  include DataMapper::Resource
  property :id,     Serial
  property :name,   String, :required => true
end

DataMapper.auto_migrate!

# Create a new record
Author.create(:name => 'Esteban')

# Fetching a record using DM
Author.first(:name => 'Esteban')

# Destroying a record
author = Author.get(7)
author.destroy

# Up next: Sequel

