# A brief look at Object-relational Mapping.
# -an ORM framework is written in an object oriented language
# (Ruby, etc.) and wrapped around a relational database.
# The object classes are mapped to the data tables in the databases
# and the object instances are mapped to rows in those tables.
#
# ORM Frameworks in Ruby:
# - ActiveRecord - the out-of-the-box ORM that comes with Rails.
#   - Follows the Active Record Design Pattern
#   - It plays the "Model" role in the MVC architecture employed by Rails
#
#
# Basic Usage of Active Record:
require 'rubygems'
require 'activerecord'
ActiveRecord::Base.establish_connection(
  :adapter => "sqlite3",
  :host => "localhost",
  :database => "articles"
)

class Article < ActiveRecord::Base
end

# Creating a new article.
Article.create(title:'ORM - ActiveRecord', author: 'Esteban')

# Fetching an article
article = Article.find(:first)

# Destroy an article
Article.find(:first).destroy

# The major disadvantage of ActiveRecord is that it breaks the
# Single Responsibility Principle (SRP).  Since the domain objects
# also handle persistence, an extra responsibility, the complexity of
# the objects increase.

# Next Up: Datamapper













