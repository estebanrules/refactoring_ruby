
array = ["Ruby", "Pascal", "Smalltalk", "io"]

# And then we want to return the value at Index 1:

array[1]

# vs.

puts array[1]

# Strictly speaking. is the puts method returning the value
# at Index 1, or is it returning the string representation
# of the value at Index 1?  On top of that, the value at
# Index 1 is a string already, so does it really matter?
