#5. Object Orientation

# a. Create a bare bones class definition for a School class.
# b. Define an initialize method for the School class.
#   i. Give your School class the instance variables: name, location, ranking,
#      students, instructors.
# NOTE: These variables should be of the same type as they are in the hash above.

#  ii. Rewrite your initialize method definition to take a parameter for each
#      instance variable.

#  iii. Initialize each instance variable with the value of the corresponding
#        parameter.
# c. Create an attr_accessor for name, location, instructors, and students.
#    Create an attr_reader for ranking.
# d. Create a method to set ranking, given a ranking value.
# e. Create a method to add a student to the school, given a name, a grade, and a semester.
# f. Create a method to remove a student from the school, given a name.
# g. Create an array constant SCHOOLS that stores all instances of your School class.
# h. Create a class method `reset` that will empty the SCHOOLS constant.
require 'pp'


class School

# Any errors that our application raises should be specific to this application.
  class SchoolErrors < RuntimeError
  end
  class NotImplemented < SchoolErrors
    def message
      @message || "Method not yet implemented!"
    end
  end
  include Enumerable
  attr_accessor :name, :location, :instructors, :students
  attr_reader :ranking

  SCHOOLS = []

  def initialize(name={}, location={}, instructors={}, students={},
                 ranking={})
    @name        = name
    @location    = location
    @instructors = instructors
    @students    = students
    @ranking     = ranking
  end

  def [](key)
    name[key]
    location[key]
    instructors[key]
    students[key]
    ranking[key]
  end

  def []=(key, value)
    name[key]        = value
    location[key]    = value
    instructors[key] = value
    students[key]    = value
    ranking[key]     = value
  end

  def set_prop(key, value)
    name[key]        = value
    location[key]    = value
    instructors[key] = value
    students[key]    = value
    ranking[key]     = value
  end

  def each
    fail NotImplemented
  end

  def set_ranking
    raise NotImplemented
  end

  def add_student(student_name, student_grade, student_semester)
    raise "Not Implemented"
  end

  def remove_student(student_name)
    raise "Not Implemented"
  end

  def reset
    raise "Not Implemented"
  end
end

if __FILE__ == $PROGRAM_NAME
  school = School.new("Ruby School", "Town of Ruby", "Ruby Master",
                      "Ruby Padawan", 100)

  pp school
end
