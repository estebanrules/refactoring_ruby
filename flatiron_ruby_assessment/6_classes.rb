# 6. Classes
# a. Create a Student class.
# b. Refactor your School instance methods to treat Students as an array
# of objects instead of an array of hashes.
# c. Create a method in the School class that finds a student by name and
#    returns the correct Student object.
