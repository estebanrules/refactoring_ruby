# 1. Arrays
require 'pp'

array = ["Blake","Ashley", "Jeff"]

#  a. Add a element to an array
array << "Esteban"

#  b. Write a statement to print out all the elements of the array.
array.each { |names| pp names }

#  c. Return the value at index 1.
array[1]

#  d. Return the index for the value "Jeff".
array.find_index("Jeff")
