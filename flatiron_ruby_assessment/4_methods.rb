#4. Methods
<<<<<<< HEAD
# Note: You will need to pass the school variable to each of these methods to
# include it in scope.
require_relative "nested_structures.rb"
=======
# TODO:  Write tests for this exercise!

SCHOOL = {
  :name => "Happy Funtime School",
  :location => "NYC",
  :instructors => [
    {:name=>"Blake", :subject=>"being awesome" },
    {:name=>"Ashley", :subject=>"being better than blake"},
    {:name=>"Jeff", :subject=>"karaoke"}
  ],
  :students => [
    {:name => "Marissa", :grade => "B"},
    {:name=>"Billy", :grade => "F"},
    {:name => "Frank", :grade => "A"},
    {:name => "Sophie", :grade => "C"}
  ]
}

>>>>>>> 6_flatiron_nested_structures
# a.
#  i. Create a method to return the grade of a student, given that student's
#  name.
#  ii. Then use it to refactor your work in 3.i.
def return_student_grade(student_name)
  student_name_to_return = SCHOOL[:students].select { |student| student[:name].eql?(student_name) }
  student_name_to_return.map { |student| student[:grade] }
end

# TODO:  Complete letter b.
# b.
#  i.Create a method to update a instructor's subject given the instructor and
#    the new subject.
def update_instructor_subject(instructor_name, instructor_subject)
  instructor_to_update = school[:instructors].select { |instructor| instructor[:name].eql?(instructor_name) }
  instructor_to_update.replace # ?
end

#  ii. Then use it to update Blake's subject to "being terrible".

# c.
#  i. Create a method to add a new student to the schools student array.
def add_student(student_name, student_grade)
  SCHOOL[:students] << ({:name => student_name, :grade => student_grade})
end

#  ii. Then use it to add yourself to the school students array.
add_student("Esteban", "A")

# d.
#  i. Create a method that adds a new key at the top level of the school hash,
#     given a key and a value.
def add_key_value_to_school(value, key)
  SCHOOL[value] = key
end

#  ii. Then use it to add a "Ranking" key with the value 1.
add_key_value_to_school(:Ranking, 1)

