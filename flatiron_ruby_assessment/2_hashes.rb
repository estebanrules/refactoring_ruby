# 2. Hashes
 require 'pp'

instructor = {:name=>"Ashley", :age=> 27}

# a. Add a new key for location and give it the value "NYC".
instructor[:location] = "NYC"

# b. Write a statement to print out all the key/value pairs in the hash
instructor.each { |k, v| pp "#{k} : #{v}" }

# c. Return the name value from the hash.
instructor[:name]

# d. Return the key name for the value 27.
instructor.key(27)
