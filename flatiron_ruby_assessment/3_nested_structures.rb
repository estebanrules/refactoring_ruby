#3. Nested Structures

school = {
  :name => "Happy Funtime School",
  :location => "NYC",
  :instructors => [
    {:name=>"Blake", :subject=>"being awesome" },
    {:name=>"Ashley", :subject=>"being better than blake"},
    {:name=>"Jeff", :subject=>"karaoke"}
  ],
  :students => [
    {:name => "Marissa", :grade => "B"},
    {:name=>"Billy", :grade => "F"},
    {:name => "Frank", :grade => "A"},
    {:name => "Sophie", :grade => "C"}
  ]
}

# a. Add a key to the school hash called "founded_in" and set it to the
# value 2013.
school[:founded_in] = 2013

# b. Add a student to the school's students' array.
school[:students] << ({:name => "Vic", :grade => "A+++"})


# c. Remove "Billy" from the students' array.
school[:students].delete({:name=>"Billy", :grade => "F"}) { "not found" }

# d. Add a key to every student in the students array called "semester" and assign
# it the value "Summer".
school[:students].map { |student| student.merge!({ :semester => "Summer" }) }


# e. Change Ashley's subject to "being almost better than Blake"
school[:instructors][1].replace(:name => "Ashley",
                                :subject => "being better than Blake",
                                :name => "Ashley",
                                :subject => "being almost better than Blake")

# f. Change Frank's grade from "A" to "F"
school[:students][2].replace(:name => "Frank",
                             :grade => "A",
                             :name => "Frank",
                             :grade => "F")

# g. Return the name of the student with a "B".
student_grade_b = school[:students].select { |student| student[:grade].eql?("B") }
student_grade_b.map { |student| student[:name] }

# h. Return the subject of the instructor "Jeff".
instructor_name_jeff = school[:instructors].select { |instructor| instructor[:name].eql?("Jeff") }
instructor_name_jeff.map { |instructor| instructor[:subject] }

# i. Write a statement to print out all the values in the school.
school.each { |k, v| puts "#{k}: #{v}" }


