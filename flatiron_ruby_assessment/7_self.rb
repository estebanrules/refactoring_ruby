# 7. Self

# For this section, please use the letters and answer each individually.
# Note: in cases where self is an instance of an object just note that as the object id printed to the screen is going to be different everytime

# What should this Class print to the screen when defined/loaded?

class Student

  def self.say_hello
    puts "hello"
  end

  say_hello
  puts self

end

class Student

  def self.say_hello
    puts self
  end

  say_hello

end

# What should this Class print to the screen when defined/loaded?

class Student

  def initialize
    puts self
  end

  new

end

# What should this code print to the screen when run?

class Student

  def say_hello
    puts self
  end

end

Student.new.say_hello

# What should this code print to the screen when run?

class Student

  def say_hello
    puts say_goodbye
  end

  def say_goodbye
    "goodbye"
  end

end

Student.new.say_hello
