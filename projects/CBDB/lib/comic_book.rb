# file: lib/comic_book.rb
require 'sqlite3'
class ComicBook
  include Enumerable

  T = Time.now
  TIME_FORMATTED = T.strftime("Added on %m/%d/%y at %H:%M")

  DB = SQLite3::Database.new("db.sqlite3")
  DB.results_as_hash = true

  PROMPT = {
    title: 'What is the title of the comic book?',
    issue_number: 'What is the issue number of the comic book?',
    publisher: 'Who is the publisher of the comic book?',
    date_added: "#{TIME_FORMATTED}",
  }

  attr_accessor :title, :issue_number, :publisher, :date_added

  def initialize(args={})
    @title        = args[:title]
    @issue_number = args[:issue_number]
    @publisher    = args[:publisher]
    @date_added   = args[:date_added]
  end

  def to_s
    "Comic Book Information:\nTitle: #{@title}\nIssue Number: ##{@issue_number}\nPublisher: #{@publisher}\nDate Added: #{@date_added}\n"
  end

  def each
    yield @title #"Title : #{@title}"
    yield @issue_number #"Issue Number: #{@issue_number}"
    yield @publisher #"Publisher : #{@publisher}"
    yield @date_added
  end
  # TODO: create a respond_to? method that correlates to your method missing.
  def method_missing(m, *args, &block)
    # accept and process any 'method call' that has pattern 'something_prompt'
    processed_method_name = m.to_s.sub!('_prompt', '')
    if PROMPT.has_key?(processed_method_name.intern)
      return PROMPT[processed_method_name.intern]
    else
      puts "This class does not contain the #{m} method."
    end
  end

  def prompt
    PROMPT
  end
end

=begin
doctest: I can present a prompt by using method missing and finding :title
>> cb = ComicBook.allocate
>> cb.title_prompt
=> "What is the title"
=end
