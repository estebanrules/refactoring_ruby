# file: lib/database.rb
require 'sqlite3'
require_relative "comic_book.rb"
# TODO: add rescue/exception.
puts "OK" if defined?(SQLite3::Database)


def disconnect_and_quit
  ComicBook::DB.close
  puts "Database is closed."
  exit
end

# TODO: add a conditional statement that informs the user if
# the table already exists.
def create_table
  ComicBook::DB.execute %q{
                          CREATE TABLE IF NOT EXISTS comic_books (
                          id integer primary key,
                          title varchar(50),
                          issue_number integer,
                          publisher varchar(20),
                          date_added varchar(50))
                          }

end

def add_comic_book
  cb = ComicBook.new(title: nil,
                     issue_number: nil,
                     publisher: nil)

  puts cb.title_prompt
  cb.title = gets.chomp

  puts ComicBook::PROMPT[:issue_number]
  cb.issue_number = gets.chomp

  puts cb.prompt[:publisher]
  cb.publisher = gets.chomp
  cb.date_added = ComicBook::TIME_FORMATTED

  ComicBook::DB.execute("INSERT INTO comic_books (title, issue_number, publisher, date_added) VALUES (?, ?, ?, ?)",
                        cb.title, cb.issue_number, cb.publisher, cb.date_added)
end

# modified query to search for title AND issue_number
def find_comic_book
  puts "Enter the title of the Comic Book to find:"
  cb_find_title = gets.chomp

  puts "Enter the issue number of the Comic Book to find:"
  cb_find_issue_number = gets.chomp

  comic_book = ComicBook::DB.execute("SELECT * FROM comic_books WHERE title = ? AND
                                     issue_number = ?", cb_find_title, cb_find_issue_number.to_i).first

  unless comic_book
    puts "No results found."
    return
  end

  print <<-COMIC_BOOK_INFO
    ---------------------------------------
    Title: #{comic_book['title']}
    Issue Number: #{comic_book['issue_number']}
    Publisher: #{comic_book['publisher']}
    Date Added: #{comic_book['date_added']}
    ---------------------------------------
  COMIC_BOOK_INFO

end

# TODO: This method is doing WAY too much.
# TODO: Implement a check to make certain comic book was modified.
def modify_comic_book
  puts "Enter the title of the comic book you would like to modify"
  cb_find_title = gets.chomp
  puts "Enter the issue number of the comic book you would like to modify"
  cb_find_issue_number = gets.chomp

  puts "Enter the modified title of the comic book"
  cb_modified_title = gets.chomp
  puts "Enter the modified issue number of the comic book"
  cb_modified_issue_number = gets.chomp
  puts "Enter the modified publisher of the comic book"
  cb_modified_publisher = gets.chomp

  ComicBook::DB.execute("UPDATE comic_books set title = ?, issue_number = ?,
                        publisher = ? WHERE title = ? AND issue_number = ?",
                        cb_modified_title, cb_modified_issue_number.to_i,
                        cb_modified_publisher, cb_find_title,
                        cb_find_issue_number.to_i)
end

# TODO: implement a check to make certain the comic book has been deleted
def delete_comic_book
  puts "What is the title of the Comic Book you would like to delete?"
  cb_delete_title = gets.chomp
  puts "What is the issue number of the Comic Book you would like to delete?"
  cb_delete_issue_number = gets.chomp

  ComicBook::DB.execute("DELETE FROM comic_books WHERE title = ? AND
                        issue_number = ?", cb_delete_title, cb_delete_issue_number.to_i).first

  puts "Comic Book Deleted."
end

# To Vic:  This code is functional, but clearly has issues.
# I plan on using ActiveRecord instead of the sqlite3 interface (not that it
# will solve all the issues, but it makes working with the database much easier.)
