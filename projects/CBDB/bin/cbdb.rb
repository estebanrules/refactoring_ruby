# file: bin/cbdb.rb
require_relative "../lib/comic_book.rb"
require_relative "../lib/database.rb"
require 'sqlite3'


def menu
  loop do
    print <<-MENU

    Please select an option:
    ---------------------------
    1. Create comic_books table
    2. Add a Comic Book
    3. Look for a Comic Book
    4. Modify a Comic Book
    5. Delete a Comic Book
    6. Quit
    ---------------------------
  MENU
  # TODO:  replace case statement with lambda/proc
  case gets.chomp
  when '1'
    create_table
  when '2'
    add_comic_book
  when '3'
    find_comic_book
  when '4'
    modify_comic_book
  when '5'
    delete_comic_book
  when '6'
    disconnect_and_quit  
  end
  end
end

menu
