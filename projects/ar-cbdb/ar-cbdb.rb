# Basic SQLite / ActiveRecord Tutorial
# First, we load the active_record/sqlite module
require 'active_record'

# Here we specify a destination to save our log.
# We could also send the log directly to the terminal using:
# Logger.new(STDERR)
ActiveRecord::Base.logger = Logger.new(File.open('database.log', 'w'))

# Finally, we actually connect to the SQLite DB.  We could also store
# data all in memory (though it wouldn't persist) via:
# :database => ':memory'
# OMG!  It's just a hash! Rails really IS magic!
ActiveRecord::Base.establish_connection(
  :adapter => 'sqlite3',
  :database => 'example.db'
)

# So we need to define a schema.  A Schema helps dictate in code what the
# structure of the database should be and what the different columns/rows
# and data types should be used.
# OMG, it's a loop and a block! Rails really IS magic!
ActiveRecord::Schema.define do
  unless ActiveRecord::Base.connection.tables.include? 'comics'
    create_table :comics do |table|
      table.column :title,         :string
      table.column :issue_number,  :integer
      table.column :publisher,     :string
    end
  end

# So what did we do above?
#
# 1 - We're defining a new Schema.
# 2 - Second, we're creating two tables: albums and tracks (which we wrap
# the creation of in a conditional statement so we make sure we don't cause
# any errors by trying to create a table that already exist.)  FYI, the
# convention is to name the tables as plurals (notice we don't call it
# it album or track - as they'll be holding multiples of that data)
# 3 - Thirdly, inside of the table creation we're specifying columns and what
# their data types should be.
#
# When we create a table a "primary key" is automatically created for us and
# is named after the table.  So in this instance we have two primary keys
# created for us: albums_id and tracks_id (again, notice the naming convention
# of tableName_id - singular, not plural)
#
# Inside the tracks table you'll see the first column we create is actually  a
# "foreign key"because we're creating a column which is named after the
# 'albums' table's primary key (album_id)

# Creating
# The ActiveRecord pattern is based on conventions, so in this instance we'll
# create two new classes that inherit from ActiveRecord's base class and we'll
# use these two classes for creating new records for each table...
class Comic < ActiveRecord::Base
end

unless Comic.find_by_title('Superman') and Comic.find_by_issue_number(1)
  comic = Comic.create(
    :title => 'Superman',
    :issue_number => 1,
    :publisher => 'DC'
  )
end

begin
   p Comic.find('Superman').title

  rescue ActiveRecord::RecordNotFound
    p 'We just rescued a "RecordNotFound" error'
end
end
