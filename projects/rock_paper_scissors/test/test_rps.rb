# TODO: Add more tests!
require_relative 'test_helper'
require_relative '../lib/rps'

class TestRockPaperScissors < Minitest::Test
  context 'a rock, paper, scissors game' do
    setup do
      @rps_game = RockPaperScissors.new
    end

    should 'have rock as an attribute assigned to a range of 1 - 3' do
      assert_equal (1..3), @rps_game.rock
    end

    should 'have paper as an attribute assigned to a range of 4 - 6' do
      assert_equal (4..6), @rps_game.paper
    end

    should 'have scissors as an attribute assigned to a range of 7 - 9' do
      assert_equal (7..9), @rps_game.scissors
    end

    # NOTE: Perhaps the is another assert method that would match a range
    # of objects?  Because this, unfortunately does not work
    should 'properly generate random numbers between 1 and 9' do
       @rps_game.random_number_generator
       assert_equal rand(1..9), @rps_game.random_number
    end
  end
end
