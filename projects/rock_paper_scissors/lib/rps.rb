# Rock, Paper Scissors
class RockPaperScissors
  include Enumerable

  attr_accessor :rock, :paper, :scissors, :random_number, :player_weapon,
                :computer_weapon

  def initialize(rock: (1..3), paper: (4..6), scissors: (7..9))
    @rock = rock
    @paper = paper
    @scissors = scissors
  end

  def each
    yield "rock: #{@rock}, paper: #{@paper}, scissors: #{@scissors}"
  end

  def rock_vs_paper
    # paper wins
  end

  def rock_vs_scissors
    # scissors wins
  end

  def paper_vs_scissors
    # scissors wins
  end

  def tie
    # tie
  end

  def random_number_generator
    @random_number = rand(1..9)
  end

  def determine_weapon
    raise "Not yet implemented"
  end
end
