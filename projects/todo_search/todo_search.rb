# Write a script that searches all files in a directory
# (and all sub-directories) for instances of the word TODO
# and/or NOTE and displays the lines that follow it.

# The following code LISTS all *.rb files in the directory
# and sub-directories
file_search = File.join("**", "*.rb")
ruby_files = Dir.glob(file_search)

puts ruby_files

# The following code finds only TODO in a single file.
File.open("1.rb") do |f|
  f.each_line do |line|
    if line =~ /TODO/
      puts "Found TODO: #{line}"
    end
  end
end

# Put it all together
