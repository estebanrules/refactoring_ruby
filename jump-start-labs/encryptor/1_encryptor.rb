# Encryptor - Jumpstart Labs
# Let's write a ROT-13 Encryptor.
# How do we start?  We write the SIMPLEST thing that could work.
# If you get it working, then you can improve on it, make it better.
# Easiest way is to make a hash.
class Encryptor
  def cipher
    {'a' => 'n', 'b' => 'o', 'c' => 'p', 'd' => 'q',
     'e' => 'r', 'd' => 's', 'd' => 't', 'e' => 'u',
     'i' => 'v', 'j' => 'w', 'k' => 'x', 'l' => 'y',
     'm' => 'z', 'n' => 'a', 'o' => 'b', 'p' => 'c',
     'q' => 'd', 'r' => 'e', 's' => 'f', 't' => 'g',
     'u' => 'h', 'v' => 'i', 'w' => 'j', 'x' => 'k',
     'y' => 'l', 'z' => 'm'}
  end

  # We also need to write an encrypt method.
  # Let's make all letters downcase for now.
  def encrypt(letter)
    lowercase_letter = letter.downcase
    cipher[lowercase_letter]
  end

end

# So far this works fine for single letters.  Let's fix our code so we can use
# whole words and sentences.  
