# Encryptor - Jumpstart Labs
# Let's write a ROT-13 Encryptor.
# How do we start?  We write the SIMPLEST thing that could work.
# Easiest way is to make a hash.
# Continued from Part 1 -
# the easiest way to encrypt whole words
# is to split the strings we are passing in to the method
# in to an Array.
class Encryptor
  def cipher
    {'a' => 'n', 'b' => 'o', 'c' => 'p', 'd' => 'q',
     'e' => 'r', 'd' => 's', 'd' => 't', 'e' => 'u',
     'i' => 'v', 'j' => 'w', 'k' => 'x', 'l' => 'y',
     'm' => 'z', 'n' => 'a', 'o' => 'b', 'p' => 'c',
     'q' => 'd', 'r' => 'e', 's' => 'f', 't' => 'g',
     'u' => 'h', 'v' => 'i', 'w' => 'j', 'x' => 'k',
     'y' => 'l', 'z' => 'm'}
  end

  # We also need to write an encrypt method.
  # Let's make all letters downcase for now.
  def encrypt_letter(letter)
    lowercase_letter = letter.downcase
    cipher[lowercase_letter]
  end

  def encrypt(string)
    # 1 - Cut the input string into letters.
    letters = string.split("")

    # 2 - Encrypt those letters one at a time, gathering the results.
    results = []
    letters.each do |letter|
      encrypted_letter = encrypt_letter(letter)
      results.push(encrypted_letter)
    end

    # 3 - Join the results back together.
    results.join
  end

end

# This works!  But we can refactor this further.
