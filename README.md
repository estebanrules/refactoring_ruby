## The Refactoring Ruby Project

This repository contains a number of exercises
and tutorials ranging from the beginner level to advanced.

The material is from various sources, including (but
not limited to):

* Jumpstart Labs
* Learn How to Program by Chris Pine
* The Pickaxe Book by Dave Thomas
* Exercises from codewars.com
* rubylearning.org
* The Flatiron School
* Kickstart Ruby
* The Bastard's Book of Ruby
* zetcode.com

As the name implies, the code is often refactored
as necessary after it is committed.
